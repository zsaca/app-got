package com.android.gotapp

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import com.android.gotapp.permission.PermissionHandler
import com.android.gotapp.permission.PermissionHandlerImpl
import kotlinx.android.synthetic.main.activity_start.*

class StartActivity : AppCompatActivity() {

    companion object {

        const val TAG: String = "StartActivity"
        const val BUNDLE_KEY_HERO: String = "start_activity_bundle_key_hero"
        const val INTENT_EXTRA_BUNDLE: String = "start_activity_intent_extra_bundle"
    }

    private lateinit var permissionHandler:PermissionHandler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)
        permissionHandler = PermissionHandlerImpl(this)
        Log.d(TAG, "onCreate() called")
        start_game_button.setOnClickListener {
            startBattleActivity(start_hero_text.text.toString())
        }
        start_hero_image.setOnClickListener {
            showWelcomeDialog()
        }
    }

    private fun startBattleActivity(name: String) {
        val intent = Intent(this, BattleActivity::class.java)
        val args = bundleOf(BUNDLE_KEY_HERO to name)
        intent.putExtra(INTENT_EXTRA_BUNDLE, args)
        startActivity(intent)
    }

    private fun showWelcomeDialog() {
        AlertDialog.Builder(this)
            .apply {
                this.setTitle(R.string.start_dialog_title)
                this.setMessage(R.string.start_dialog_message)
                this.setPositiveButton(R.string.start_dialog_ok) { _, _ -> permissionHandler.requestWriteStoragePermission() }
                this.setNegativeButton(R.string.start_dialog_cancel, null)
            }.show()
    }

    override fun onStart() {
        super.onStart()
        Log.d(TAG, "onStart() called")
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume() called")
    }

    override fun onPause() {
        super.onPause()
        Log.d(TAG, "onPause() called")
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG, "onStop() called")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy() called")
    }
}
