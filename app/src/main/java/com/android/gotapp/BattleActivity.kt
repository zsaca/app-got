package com.android.gotapp

import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_battle.*
import java.util.concurrent.Callable
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors


class BattleActivity : AppCompatActivity() {

    companion object{
        const val TAG: String = "BattleActivity"
    }
    private lateinit var enemyAdapter: EnemyAdapter
    private val enemies: MutableList<EnemyModel> = mutableListOf()
    private var lastAddedEnemyPosition: Int = 0
    private val executorService: ExecutorService? = Executors.newFixedThreadPool(1)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_battle)
        //readBundle()
        initializeEnemyRecyclerView()
        initializeEnemies()
        //battle_add_fab.setOnClickListener { addEnemy() }
    }

    override fun onDestroy() {
        super.onDestroy()
        executorService?.shutdown()
    }

    private fun readBundle(){
        val b = intent.extras.getBundle(StartActivity.INTENT_EXTRA_BUNDLE)
        b?.let {
            val hero = it.getString(StartActivity.BUNDLE_KEY_HERO)
            Toast.makeText(this, hero, Toast.LENGTH_SHORT ).show()
        }
    }

    private fun initializeEnemyRecyclerView() {
        enemyAdapter = EnemyAdapter(object : EnemyAdapter.BattleStartListener {
            override fun startFight(life: Int) {
                when (life) {
                    1, 20 -> fightOnMainThread(life)
                    3 -> fightOnWorkerThreadWithRunnable(life)
                    4 -> fightOnWorkerThreadWithCallable(life)
                    5 -> fightOnWorkerWithAsyncTask(life)
                }
            }
        })
        battle_recycler_view.layoutManager = LinearLayoutManager(this)
        battle_recycler_view.adapter = enemyAdapter
    }

    private fun initializeEnemies() {
        enemies.add(EnemyModel("Joffrey", 1, R.drawable.es_joffrey))
        enemies.add(EnemyModel("Ramsay", 20, R.drawable.es_ramsay))
        enemies.add(EnemyModel("Cercei", 3, R.drawable.es_cersei))
        enemies.add(EnemyModel("Tywin Lannister", 4, R.drawable.es_tywin))
        enemies.add(EnemyModel("Daenerys", 5, R.drawable.es_daenerys))
    }

    private fun addEnemy() {
        if (lastAddedEnemyPosition <= 4) {
            enemyAdapter.addEnemy(enemies[lastAddedEnemyPosition])
            lastAddedEnemyPosition++
        }
    }

    private fun fight(life: Int) {
        val t = Thread.currentThread()
        Log.d(TAG, t.name)
        Thread.sleep((life * 1000).toLong())
    }

    //Calling fight on UI thread freezes the app
    private fun fightOnMainThread(lifeValue: Int) {
        battle_progress.visibility = View.VISIBLE
        fight(lifeValue)
        battle_progress.visibility = View.GONE
    }

    //progress state is set right after calling worker thread, so we can't see anything
    private fun fightOnWorkerThreadWithRunnable(lifeValue: Int) {
        battle_progress.visibility = View.VISIBLE
        executorService!!.submit(Runnable {
            fight(lifeValue)
        })
        battle_progress.visibility = View.GONE
    }

    //also can't see progress bar? What happens when calling result.get(). Actually it is waiting for result
    //and freezing meanwhile the ui thread.
    //We used it rather in a service (IntentService) which runs in a background thread
    private fun fightOnWorkerThreadWithCallable(lifeValue: Int) {
        battle_progress.visibility = View.VISIBLE
        val result = executorService!!.submit(Callable<Any> {
            fight(lifeValue)
            Any()
        })
        if (result.get() != null) {
            battle_progress.visibility = View.GONE
        }
    }

    //Working good
    private fun fightOnWorkerWithAsyncTask(lifeValue: Int) {
        FightTask().execute(lifeValue)
    }

    private inner class FightTask : AsyncTask<Int, Any, Any>() {

        override fun onPreExecute() {
            battle_progress.visibility = View.VISIBLE
        }

        override fun doInBackground(vararg params: Int?): Any {
            params[0]?.let { fight(it) }
            return Any()
        }

        override fun onPostExecute(result: Any?) {
            battle_progress.visibility = View.GONE
        }
    }
}
