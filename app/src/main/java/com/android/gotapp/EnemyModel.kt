package com.android.gotapp

data class EnemyModel(
    val name: String,
    val lifeValue: Int,
    val imageResId: Int
)