package com.android.gotapp.permission

interface PermissionHandler {

    fun requestWriteStoragePermission()
}