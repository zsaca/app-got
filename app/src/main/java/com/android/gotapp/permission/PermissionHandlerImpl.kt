package com.android.gotapp.permission

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat

class PermissionHandlerImpl(val activity:Activity):PermissionHandler {

    companion object{
        const val WRITE_STORAGE_PERMISSION_REQUEST_CODE:Int = 101
    }

    override fun requestWriteStoragePermission() {
       if(!isWriteStoragePermissionGranted()){
           ActivityCompat.requestPermissions(
               activity,
               arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
               WRITE_STORAGE_PERMISSION_REQUEST_CODE
           )
       }
    }

    private fun isWriteStoragePermissionGranted(): Boolean {
        return ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
    }

}