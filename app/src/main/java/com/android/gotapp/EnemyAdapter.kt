package com.android.gotapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_enemy_item.view.*

class EnemyAdapter(listener: BattleStartListener) : RecyclerView.Adapter<EnemyAdapter.ViewHolder>() {

    private val enemies: MutableList<EnemyModel> = mutableListOf()
    private val battleStartListener: BattleStartListener = listener

    fun addEnemy(enemy: EnemyModel) {
        enemies.add(enemy)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return enemies.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.view_enemy_item, parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(enemies[position], battleStartListener)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), LayoutContainer {

        override val containerView: View?
            get() = itemView

        fun bind(enemy: EnemyModel, listener: BattleStartListener) {
            itemView.enemy_name.text = enemy.name
            itemView.enemy_life_value.text = enemy.lifeValue.toString()
            itemView.enemy_image.setImageDrawable(containerView?.context?.getDrawable(enemy.imageResId))
            itemView.setOnClickListener {
                listener.startFight(enemy.lifeValue)
                itemView.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.red))
            }

        }
    }

    interface BattleStartListener {

        fun startFight(life: Int)
    }
}
